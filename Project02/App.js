import React, {Component} from "react";
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from "react-native";
import Message from "./components/message/Message";
import Body from "./components/body/Body";
import OurFlatList from "./components/ourFlatList/OurFlatList";
import ConexionFetch from "./components/conexionFetch/ConexionFetch";
import { isTemplateElement } from "@babel/types";

const provincias = [
  {
    id: 1,
    name: 'Arequipa',
  },
  {
    id: 2,
    name: 'Puno',
  },
  {
    id: 3,
    name: 'Cuzco',
  },
];

export default class App extends Component{

  constructor (props){
    super(props);

    this.state = {
      textValue: '',
      count: 0,
    };
  }

  changeTextInput = text => {

    console.log(text)

    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  /*render() {
    return (
      <View style={styles.container}>
        <OurFlatList />
      </View>
      ); 
    }*/
    render() {
      return <ConexionFetch />
    }
  }

  /*render(){
    return (
      <View style={styles.container}>
        <Message />

        <View style={styles.text}>
          <Text>Ingrese su edad</Text>
        </View>

        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textValue}
        />

        <Body textBody={'Texto en Body'} onBodyPress={this.onPress} />

        <View style={[styles.countContainers]}>
          <Text style={[styles.countText]}>
            {this.state.count}
          </Text>
        </View>

        {provincias.map(item => (
          <View>
            <Text>{item.name}</Text>
          </View>
        ))}
      </View>
    );
  }
}*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },  

  text: {
    alignItems: 'center',
    padding: 10,
  },

  countContainers: {
    alignItems: 'center',
    padding: 10,
  },

  countText: {
    color: '#FF00FF',
    
  },

  image: {
    alignItems: 'center'
  }
});